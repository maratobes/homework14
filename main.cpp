#include <iostream>
#include <string>

int main() {
    std::string s = "test string";
    std::cout << s << "\n";
    std::cout << "s length = " << s.length() << "\n";
    std::cout << s[0] << " " << s[s.length()-1];
    return 0;
}